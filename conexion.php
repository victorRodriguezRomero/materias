<?php
class Conexion{
    public $nombre;
    public $password;
    public $genero;
    public $email;
    public $materia;
    public $Telefono;
    public $host;
    public $dbusername;
    public $dbpassword;
    public $dbname;
    public $conn;
    
    function recibir_datos($Nombre, $Password, $Genero, $Email,
        $Materia, $Telefono, $hostn, $dbUserName, $dbPassword, $dbName){
            $this->nombre=$Nombre;
            $this->password=$Password;
            $this->genero=$Genero;
            $this->email=$Email;
            $this->materia=$Materia;
            $this->telefono=$Telefono;
            $this->host=$hostn;
            $this->dbusername=$dbUserName;
            $this->dbpassword=$dbPassword;
            $this->dbname=$dbName;
    }
        function validar_conexion(){
            $conn = new mysqli($this->host, $this->dbusername, $this->dbpassword, $this->dbname);
            if(mysqli_connect_error()){
                die('connect error('.mysqli_connect_errno().')'.mysqli_connect_error()); 
            }else{
            $this->registrar_usuario( $conn);
        }
    }

        function registrar_usuario($conn){
            $SELECT="SELECT telefono from usuario where telefono = ? limit 1";
            $INSERT="INSERT INTO usuario (nombre, password, genero, email, materia, telefono)
            values(?,?,?,?,?,?)";

            $stmt = $conn->prepare($SELECT);
            $stmt-> bind_param("i", $this->telefono);
            $stmt->execute();
            $stmt->bind_result($this->telefono);
            $stmt->store_result();
            $rnum=$stmt->num_rows;
            if($rnum==0){
                $stmt ->close();
                $stmt = $conn->prepare($INSERT);
                $stmt-> bind_param("sssssi",$this->nombre, $this->password, $this->genero, 
                $this->email, $this->materia, $this->telefono);
                $stmt -> execute();
                echo "Registo completo";
            }
            else{
                    echo "alguien registro ese numero <br>";
            }
            $stmt->close();
            $conn->close();
    }
    
}
?>